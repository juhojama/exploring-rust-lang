#[macro_use]

extern crate prettytable;
extern crate reqwest;
extern crate select;

use prettytable::Table;
use select::document::Document;
use select::predicate::{Class, Name, Predicate};

static URL: &str = "https://news.ycombinator.com";

#[tokio::main]
async fn main() -> Result<(), reqwest::Error> {
    let res = reqwest::get(URL).await?;
    println!("Status: {}", res.status());

    let body = res.text().await?;

    let document =
        Document::from_read(::std::io::Cursor::new(body.into_bytes())).expect("Document from_read");

    let mut table = Table::new();

    for node in document.find(Class("athing")) {
        let rank = node.find(Class("rank")).next().unwrap();
        let story = node
            .find(Class("title").descendant(Name("a")))
            .next()
            .unwrap()
            .text();
        let url = node
            .find(Class("title").descendant(Name("a")))
            .next()
            .unwrap();
        let url_txt = url.attr("href").unwrap();
        let url_trim = url_txt.trim_start_matches('/');
        let rank_story = format!(" | {} | {}", rank.text(), story);
        table.add_row(row![FdBybl->rank_story]);
        table.add_row(row![Fy->url_trim]);
    }
    table.printstd();

    Ok(())
}
