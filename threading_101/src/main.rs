use std::sync::mpsc;
use std::thread;
use std::time::Duration;

fn main() {
    let (tx, rx) = mpsc::channel();

    let handle = thread::spawn(move || {
        // let mut v = vec![1, 2, 3];
        let v = vec![String::from("Rust"), String::from("is"), String::from("fun!")];

        for val in v {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    let handle_2 = thread::spawn(move || {
        for i in 1..5 {
            println!("Hi number {} from the second thread!", i);
            thread::sleep(Duration::from_secs(1));
        }
    });

    for received in rx {
        println!("Received {:?}", received);
    }

    handle.join().unwrap();
    handle_2.join().unwrap();

    println!("End of threads");

}


