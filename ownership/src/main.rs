fn main() {
    // Cloning variable
    let mut s1 = String::from("Hello");
    let s2 = s1.clone();
    println!("{} {}", s1, s2);

    // Passing ownership
    s1 = gives_ownership();
    let mut s3 = takes_ownership_and_gives_back(s1);
    println!("{}", s3);

    // References
    let len: usize = calculate_length(&s3);
    println!("The length of the string is: {}", len);

    change_borrowed(&mut s3);
    
    // Slice
    let s4 = String::from("First word");
    let first_word_index = find_first_word_index(&s4);
    let first_word = find_first_word(&s4);
    println!("First word index is '{}'", first_word_index);
    println!("First word is '{}'", first_word);

}

pub fn gives_ownership() -> String {
    let string = String::from("I am a String");
    string
}

pub fn takes_ownership_and_gives_back(string: String) -> String {
    string
}

pub fn calculate_length(s: &String) -> usize {
    s.len()
}

pub fn change_borrowed(s: &mut String) {
    s.push_str("Added to the string");
}

pub fn find_first_word_index(s: &String) -> usize {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return i;
        }
    }

    s.len()
}

pub fn find_first_word(s: &String) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }

    &s[..]
}
