use std::io;
use std::io::prelude::*;
use std::process;

fn main() {
    print_pointers();
    let stdin = io::stdin();
    for _line in stdin.lock().lines() {
        println!("#################################");
        print_pointers()
    }
}

fn print_pointers() {
    let var_int = &12345;

    let var_string = "DefaultString";
    let mut array_char = Vec::with_capacity(128);
    array_char.resize(128, "");

    let ptr2int = format!("{:p}", var_int);
    let ptr2ptr = &ptr2int;
    let _ptr2ptr2 = &ptr2ptr;

    // Creating a raw pointer to an arbitrary memory address
    let address = 0x012345usize;
    let _r = address as *const i32;

    let num = 5;
    let p = &num as *const i32;

    // Dereferencing in unsafe block
    unsafe {
        println!("p is: {}", *p);
    
    }

    println!("Process ID is {}", process::id());
    println!("String: {}", var_string);
    println!("Pointer to string: ({:p})", var_string);
    println!("string pointer: ({:p})", &var_string);
    println!("Pointer to int: ({:p}) = ({:p})", &ptr2int, var_int);
    println!("Pointer to pointer: ({:p}) = {:p}", &ptr2ptr, &ptr2int);
    println!("Press ENTER to print again");

}
