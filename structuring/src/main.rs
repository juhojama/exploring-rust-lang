pub struct User {
    username: String,
    active: bool,
}

fn main() {

    let mut user = User {
        username: String::from("hello@world.com"),
        active: true,
    };

    user.username = String::from("new@username.com");
    user.active = false;

    let user2 = build_user(String::from("username"), false);

}

pub fn build_user(username: String, active: bool) -> User {
    User {
        username,
        active,
    }
}
