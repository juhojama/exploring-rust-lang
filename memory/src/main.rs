use std::io;
use std::io::prelude::*;

extern crate read_process_memory;

use read_process_memory::{copy_address, Pid, TryIntoProcessHandle};

pub fn read_bytes(pid: Pid, address: usize, size: usize) -> Option<Vec<u8>> {
    let handle = pid.try_into_process_handle().ok()?;

    // Read the bytes
    copy_address(address, size, &handle).ok()
}

pub fn read<T: Sized>(pid: Pid, address: usize) -> Option<T> {
    let bytes = read_bytes(pid, address, std::mem::size_of::<T>())?;
    Some(unsafe { (bytes.as_ptr() as *const T).read() })
}

fn main() {
    let pid = 22612 as _;

    // Reading string
    let address = 0x7ff73e5088ac;

    println!("Address: {}", address);

    let bytes = read_bytes(pid, address, 13).unwrap_or_default();
    let string = std::str::from_utf8(bytes.as_slice()).expect("Slice is not UTF-8");
    let s = String::from_utf8_lossy(&bytes);
    println!("Bytes: {:?}", bytes);
    println!("Converted to string: {}", s);
    println!("String: {}", string);

    // Reading int
    println!("var_int: {:?}", read::<u32>(pid, 0x95dd6ff3c4));

    // Reading pointer to int
    let pointer = read::<usize>(pid, 0x95dd6ff610).unwrap_or_default();
    println!("pointer to pointer to var_int: {:x}", pointer);

    let pointer = read::<usize>(pid, pointer).unwrap_or_default();
    println!("pointer to var_int: {:x}", pointer);

    println!("var_int: {:?}", read::<u32>(pid, pointer));
}
