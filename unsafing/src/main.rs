static mut COUNTER: i32 = 0;

extern "C" {
    fn abs(input: i32) -> i32;
}

fn add_to_count(inc: i32) {
    unsafe {

        let absolute_inc = abs(inc);
        COUNTER += absolute_inc;
    }
}

fn main() {
    add_to_count(3);
    unsafe {
        println!("COUNTER: {}", COUNTER);
        println!("Absolute value of -3 according to c is {}", abs(-3));
    }
}
