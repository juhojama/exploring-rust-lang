fn bubble_sort(arr: &mut [i32]) {
    let mut swapped = true;
    while swapped {
        swapped = false;
        let iterate_max = arr.len() - 1;
        for x in 0..iterate_max {
            if arr[x] > arr[x + 1] {
                arr.swap(x + 1, x);
                swapped = true;
            }
        }


    } {}

    println!("Printing the sorted array...");

    for x in arr {
        println!("{}", x);
    }

}

fn main() {
    let mut arr: [i32; 4] = [3, 2, 5, 1];

    bubble_sort(&mut arr);

}
